#include <stdio.h>
 
int logicfcn( int a ) {
    int i = a >> 4;
    unsigned int b = i & 255;
    printf("%hhx\n", b);
    printf("%hhu\n", b);
}
 
int main() {
    int a = 43981;
    logicfcn(a);
}