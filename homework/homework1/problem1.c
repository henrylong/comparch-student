#include <stdio.h>

int main() {
	printf("Hex: 0x%1$hhx \tUnsigned: %1$hhu \tSigned: %1$hhd\n", -85);
	printf("Hex: 0x%1$hhx \tUnsigned: %1$hhu \tSigned: %1$hhd\n", 18);
	printf("Hex: 0x%1$hhx \tUnsigned: %1$hhu \tSigned: %1$hhd\n", -51);
}