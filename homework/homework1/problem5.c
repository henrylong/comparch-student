#include <stdio.h>
#include <math.h>

int concat(short m, short n);

int main() {
	short m, n;
	int x;
	m = 0x00;
	n = 0x01;
	x = concat(m, n);
	printf("%d\n", x);

	m = 0x01;
	n = 0x01;
	x = concat(m, n);
	printf("%d\n", x);

	m = 0x02;
	n = 0x01;
	x = concat(m, n);
	printf("%d\n", x);

	m = 0x00;
	n = 0x02;
	x = concat(m, n);
	printf("%d\n", x);
}

int concat(short m, short n){
	int array[8];
	int i;
	for (i = 3; i >= 0; i--){
		array[i] = m % 2;
		m = m / 2;
	}
	for (i = 3; i >= 0; i--){
		array[i + 4] = n % 2;
		n = n / 2;
	}
	int num = 0;
	int baseNum;
	for (i = 7; i >= 0; i--){
		baseNum = 2 * array[7 - i];
		if (i){
			num += (int) pow(baseNum, i);
		} else{
			if (array[7 - i]){
				num += 1;
			}
		}
	}
	return num;
}