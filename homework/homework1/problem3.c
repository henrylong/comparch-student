#include <stdio.h>

unsigned bitrange(unsigned inst, unsigned hi, unsigned lo);

int main() {
	unsigned inst;
	inst = 0xabcd1234;
	bitrange(inst, 3, 0);
	bitrange(inst, 31, 28);
	bitrange(inst, 27, 4);
	bitrange(0xffcd4321, 27, 24);
	bitrange(0x22222222, 31, 8);
}

unsigned bitrange(unsigned inst, unsigned hi, unsigned lo) {
	inst = inst << (31 - hi);
	inst = inst >> (31 - hi + lo);
	printf("0x%x\n", inst);
	return 0;
}