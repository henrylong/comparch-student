#include <stdio.h>

int signext(int, unsigned);

int main() {
	signext(0x8f, 8);
	signext(0x18f, 12);
	signext(0x8f, 9);
	signext(0xabcd, 16);
}

int signext(int n, unsigned s){
	if (n >> (s - 1) == 1) {
		n = (0xffffffff << s) | n;
	}
	printf("0x%08x\n", n);
	return n;
}