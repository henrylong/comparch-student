#include <stdio.h>

void sumFunc(int array[], int n, int *);

int main() {
	int array[] = {17, -3, 8, 49, -5, 56, 100, -22};
	int sum = 0;
	int *sumP = &sum;
	sumFunc(array, 4, sumP);
	printf("%d\n", *sumP);
	sum = 0;
	sumFunc(array, 6, sumP);
	printf("%d\n", *sumP);
}

void sumFunc(int array[], int n, int *sumP){
	int i;
	for (i = 0; i < n; i++) {
		*sumP += array[i];
	}
}