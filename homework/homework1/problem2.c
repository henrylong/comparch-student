#include <stdio.h>

void printbin(char);

int main(){
  printbin(-128);
  printbin(-65);
  printbin(-5);
  printbin(-1);
  printbin(0);
  printbin(1);
  printbin(5);
  printbin(65);
  printbin(127);
}

void printbin(char n){
  int array[8];
  int i, z;
  z = n;
  for(i = 0; i < 8; i++){
    array[i] = n % 2;
    n = n / 2;
  }
  if (z < 0){
    for (i = 0; i < 8; i++){
      if (array[i] == 0){
        array[i] = 1;
      } else array[i] = 0;
    }
  for (i = 0; i < 8; i++){
    if (array[i] == 0){
      array[i] = 1;
      break;
    } else array[i] = 0;
  }  
  }
  printf("%d:   ", z);
  for(i = 7; i >= 0; i--){
    printf("%d", array[i]);
  }
  printf("\n");
}
